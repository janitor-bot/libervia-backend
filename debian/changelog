libervia-backend (0.8.0-2) unstable; urgency=medium

  * Source-only upload.

 -- Martin <debacle@debian.org>  Mon, 14 Feb 2022 13:55:42 +0000

libervia-backend (0.8.0-1) unstable; urgency=medium

  * New upstream release.
  * Project is renamed to "Libervia".

 -- Martin <debacle@debian.org>  Thu, 13 Jan 2022 16:41:40 +0000

salutatoi (0.8.0~hg3453.864485605d12-3) unstable; urgency=medium

  * Build depend on setuptools_scm and drop patch to stop requiring it.
  * Stop shipping Twisted license and README.

 -- Thomas Preud'homme <robotux@debian.org>  Sun, 31 Jan 2021 21:52:06 +0000

salutatoi (0.8.0~hg3453.864485605d12-2) unstable; urgency=medium

  * Source-only upload.

 -- Thomas Preud'homme <robotux@debian.org>  Sat, 23 Jan 2021 11:01:19 +0000

salutatoi (0.8.0~hg3453.864485605d12-1) unstable; urgency=medium

  * New upstream snapshot (Closes: #971291):
    + Update sat_tmp
    + Rebase debian/patches
    + Replace python3-crypto by python3-pycryptodome
    + Remove patch for sat.sh which no longer exists
    + Update installation instructions to work with new sat launcher script and
      create symlink in /usr/bin
  * Make setuptools installation verbose by default and honor terse option in
    DEB_BUILD_OPTIONS.
  * Bump standard version to 4.5.1.
  * debian/rules:
    + Stop setting no longer used SAT_INSTALL
    + Simplify logic
  * Document intentionally not installed files.
  * Bump debhelper compat to 13.
  * debian/watch: Migrate to format v4 and switch to https.

 -- Thomas Preud'homme <robotux@debian.org>  Wed, 20 Jan 2021 23:21:13 +0000

salutatoi (0.8.0~hg3247.f981c0e99220-2) unstable; urgency=medium

  * Removed obsolete dependencies on pyfeed and xmlelements

 -- Martin <debacle@debian.org>  Sun, 05 Apr 2020 09:11:49 +0000

salutatoi (0.8.0~hg3247.f981c0e99220-1) unstable; urgency=medium

  * New upstream snaphost, move to Python 3 (Closes: #944425)

 -- Martin <debacle@debian.org>  Sat, 04 Apr 2020 23:46:37 +0000

salutatoi (0.7.0a4-1) unstable; urgency=medium

  * New upstream (alpha) release

 -- W. Martin Borgert <debacle@debian.org>  Fri, 01 Mar 2019 19:48:01 +0000

salutatoi (0.7.0a3-1) unstable; urgency=medium

  * New upstream (alpha) release

 -- W. Martin Borgert <debacle@debian.org>  Wed, 06 Feb 2019 11:14:00 +0000

salutatoi (0.7.0a1+hg20180914.f69c1b260e49-2) unstable; urgency=medium

  * Remove obsolete dependency on python-gobject-2 (Closes: #912954)

 -- W. Martin Borgert <debacle@debian.org>  Mon, 05 Nov 2018 10:50:46 +0000

salutatoi (0.7.0a1+hg20180914.f69c1b260e49-1) unstable; urgency=medium

  * New upstream snapshot
  * Update patches (search mo files, set PYTHONPATH)
  * Remove obsolete patch (prevent symlink creation)
  * Add patch (no use of setuptools_scm)
  * Update sat-tmp to latest upstream 0.7.0a2
  * Update depends and recommends

 -- W. Martin Borgert <debacle@debian.org>  Wed, 26 Sep 2018 22:18:45 +0000

salutatoi (0.6.1.1+hg20180228-1) unstable; urgency=medium

  * New upstream snapshot.

 -- W. Martin Borgert <debacle@debian.org>  Thu, 01 Mar 2018 17:22:30 +0000

salutatoi (0.6.1.1+hg20180208-1) unstable; urgency=medium

  * New upstream release.
  * Primitivus depends on new python-urwid-satext (Closes: #891148).
  * Move to XMPP team and salsa.debian.org, add myself to uploaders.
  * Bump standards and dh compat level.
  * Add sat_tmp wokkel patches until wokkel incorporates them.

 -- W. Martin Borgert <debacle@debian.org>  Fri, 23 Feb 2018 08:25:31 +0000

salutatoi (0.6.1-1) unstable; urgency=medium

  * New upstream release.
  * Remove generated files when cleaning.
  * debian/control:
    + update versioned dependency on python-urwid-satext
    + use encrypted URLs for Vcs-* fields
    + remove sat-xmpp-wix stanza
    + bump Standards-Version to 3.9.8 (no changes needed)
  * Remove debhelper files for wix package.
  * debian/source/lintian-overrides: add override for not checking GPG in
    debian/watch since upstream tarball do not have GPG signatures.
  * Remove line for removed file constants.py in debian/sat-xmpp-core.install.
  * debian/copyright: update given latest release.

 -- Thomas Preud'homme <robotux@debian.org>  Sun, 07 Aug 2016 16:30:21 +0100

salutatoi (0.5.1-2) unstable; urgency=medium

  * Reorder Recommends of sat-xmpp-core.
  * core: add missing dependency on python-gobject-2.
  * Fix Primitivus' dependency (python-gi instead of python-gobject-2).

 -- Matteo Cypriani <mcy@lm7.fr>  Wed, 24 Sep 2014 10:38:50 -0400

salutatoi (0.5.1-1) unstable; urgency=medium

  * New upstream release.
    + Dropped patches "Avoid setting a bad icon" and "Lower default setuptools
      version", applied upstream.
  * DEP-12: remove Homepage and Watch fields, which are not recommended.
  * Improve manual pages (sat.1 and primitivus.1).

 -- Matteo Cypriani <mcy@lm7.fr>  Sat, 20 Sep 2014 23:40:26 -0400

salutatoi (0.5.0-1) unstable; urgency=medium

  * New upstream release.
    + Update dependencies.
    + Remove lintian override (sat.tac doesn't exist any more).
    + Ship the Twisted plugin and the D-Bus service file.
    + Update patch to set PYTHONPATH in sat.sh.
    + Lower the default version of python-setuptools in ez_setup.py.
    + Prevent setup.py from creating symlinks.
    + Update manual pages.
  * debian/rules: fix typo in setup.py options (dh_auto_install).
  * Update for wxPython 3.0 (Closes: #759059).
    + Add patch (contributed by Olly Betts) allowing Wix to start by not
      setting a missing icon.
  * Ship additional documentation files (README and README4TRANSLATORS).

 -- Matteo Cypriani <mcy@lm7.fr>  Sun, 14 Sep 2014 22:33:34 -0400

salutatoi (0.4.1-2) unstable; urgency=medium

  * Update Jp's manual page (jp.1).
  * Install Jp's Zsh completion file.

 -- Matteo Cypriani <mcy@lm7.fr>  Wed, 12 Mar 2014 14:12:07 -0400

salutatoi (0.4.1-1) unstable; urgency=medium

  * New upstream release.

  [ Thomas Preud'homme ]
  * Delete patches integrated upstream.

  [ Matteo Cypriani ]
  * Quit updating distribute_setup.py since it was updated upstream.
  * debian/rules: call bridge_constructor.py instead of bridge_contructor.py
    (typo fixed upstream in this file's name).
  * debian/sat-xmpp-{core,jp}.install: remove references to sat_frontend.mo
    and jp.mo (all the translations are now in sat.mo).
  * Install new files in /usr/share/salutatoi/sat_frontends:
    + constants.py (in sat-xmpp-core)
    + tools/ (in sat-xmpp-core)
    + jp/ (in sat-xmpp-jp)
  * Update dependencies.
  * Update debian/copyright.
  * Update Homepage.
  * Add a patch to allow Jp to depend on python-gi (GObject 3) instead of the
    deprecated GObject 2.

 -- Matteo Cypriani <mcy@lm7.fr>  Tue, 11 Mar 2014 13:41:50 -0400

salutatoi (0.3.0-7) unstable; urgency=medium

  * Relax sat-xmpp-primitivus' dependency on python-urwid-satext, since
    urwid-satext 0.3 is retro-compatible with Salut à Toi 0.3.

 -- Matteo Cypriani <mcy@lm7.fr>  Fri, 28 Feb 2014 10:25:29 -0500

salutatoi (0.3.0-6) unstable; urgency=medium

  * Also use newer distribute_setup.py in build target.

 -- Matteo Cypriani <mcy@lm7.fr>  Mon, 24 Feb 2014 09:16:19 -0500

salutatoi (0.3.0-5) unstable; urgency=medium

  * Use newer distribute_setup.py to work with recent python-setuptools
    (Closes: #735813).
  * Bump Standards-Version to 3.9.5 (no changes needed).

 -- Matteo Cypriani <mcy@lm7.fr>  Tue, 11 Feb 2014 10:48:21 -0500

salutatoi (0.3.0-4) unstable; urgency=low

  * debian/patches:
    + Add a patch to install sat files as private modules.
  * debian/rules:
    + Remove distribute files in clean target (Closes: #735813).

 -- Thomas Preud'homme <robotux@debian.org>  Sat, 01 Feb 2014 15:03:00 +0800

salutatoi (0.3.0-3) unstable; urgency=low

  * Upload to unstable.

  [ Thomas Preud'homme ]
  * Tighten dependencies between sat-xmpp-primitivus and python-urwid-satext
    since salutatoi and urwid-satext evolve in lockstep.
  * debian/patches:
    + Fix crash when using Salut à Toi for the first time and no profile has
      been created yet.
    + Document how to launch D-Bus on a terminal, as required by Salut à Toi.
    + Stop gracefully if bridge can't be initialized.
    + Make Wix quit completely when exited.

 -- Matteo Cypriani <mcy@lm7.fr>  Sun, 23 Jun 2013 19:28:51 -0400

salutatoi (0.3.0-2) experimental; urgency=low

  * Cherry-pick 2 patches from upstream to adapt primitivus widgets to
    urwid 1.1 API.
  * Tighten dependencies between frontend packages and core package.

 -- Thomas Preud'homme <robotux@debian.org>  Fri, 07 Jun 2013 15:13:07 +0200

salutatoi (0.3.0-1) experimental; urgency=low

  * Initial release. (Closes: #703659)

 -- Thomas Preud'homme <robotux@debian.org>  Wed, 15 May 2013 16:22:00 +0200
