ARG REVISION
FROM libervia/backend:${REVISION:-dev}

LABEL maintainer="Goffi <tmp_dockerfiles@goffi.org>"

USER root

ARG DEBIAN_FRONTEND=noninteractive

RUN mkdir -m 777 /reports && \
    apt-get install -y --no-install-recommends \
    # firefox is needed to test Libervia via selenium
    firefox-esr \
    # VNC/X11 server for visual mode + window manager
    # using openbox as it allows to resize easily when doing manual debugging/tests
    # and it adapts correctly when doing driver.set_window_size
    tigervnc-standalone-server openbox \
    # useful for remote debugging
    telnet net-tools \
    # needed to make Firefox trust system certificates
    # cf. https://askubuntu.com/a/1036637
    p11-kit && \
    ln -fs /usr/lib/x86_64-linux-gnu/pkcs11/p11-kit-trust.so /usr/lib/firefox-esr/libnssckbi.so

# we install pre-generated certificates so we can do tests with valid TLS
COPY --chown=root:root certificates/minica.pem /usr/local/share/ca-certificates/minica.crt
COPY --chown=root:tls-cert certificates/server1.test/cert.pem /usr/share/libervia/certificates/server1.test.pem
COPY --chown=root:tls-cert certificates/server1.test/key.pem /usr/share/libervia/certificates/server1.test-key.pem
RUN update-ca-certificates

COPY --chown=root:root libervia.conf /etc/libervia.conf
COPY --chown=libervia:libervia scripts/entrypoint.sh /home/libervia/entrypoint_e2e.sh

RUN \
    # we install webdriver (needed to control Firefox from Selenium)
    # note: this is not absolutely necessary as long as we use helium because it includes
    # is own webdriver
    python -c 'from urllib.request import urlretrieve;\
    urlretrieve(\
    "https://github.com/mozilla/geckodriver/releases/download/v0.28.0"\
    "/geckodriver-v0.28.0-linux64.tar.gz", "/usr/local/bin/geckodriver.tar.gz")' && \
    cd /usr/local/bin && tar zxf geckodriver.tar.gz && rm -f geckodriver.tar.gz

WORKDIR /home/libervia
USER libervia

RUN \
    # pytest and its plugins
    pip install pytest pytest-timeout pytest-dependency \
    # needed to test libervia-cli
    sh \
    # needed to test libervia
    helium \
    # needed to check sent emails
    aiosmtpd \
    # useful for debugging
    pudb

RUN ./entrypoint.sh \
    # we create the file sharing component which will autoconnect when backend is started
    li profile create file-sharing -j files.server1.test -p "" --xmpp-password test_e2e -C file-sharing -A && \
    libervia-backend stop

ENV LIBERVIA_TEST_REPORT_DIR=/reports
ENV DISPLAY=:0
USER root
RUN apt-get install -y openbox
USER libervia
EXPOSE 5900

ENTRYPOINT ["/home/libervia/entrypoint_e2e.sh"]
