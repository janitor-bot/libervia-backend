The project "Libervia" was born from a need to protect our liberties, our privacy and our independence. It is intended to protect the rights and liberties a user has regarding her own private and numeric data, her acquaintance's, and the data she handles; it is also intended to be a human contact point, not substituting itself to physical encounters, but rather facilitating them.
Libervia will always fight against all forms of technology control by private interests. The global network must belong to everybody, and be a force of expression and freedom for all Humanity.

Towards this end, "Libervia" and those who participate in the project operate on a Social Contract, a commitment to those who use it. This Contract involves the following points :

- We put the freedom at the top of our priorities : freedom of the user, freedom with her data. To achieve this, "Libervia" is a Libre Software - an essential condition - and its infrastructure also relies on Libre Software, meaning softwares that respect the 4 fundamental rules :
    - The freedom to run the program for any purpose.
    - The freedom to study how the program works, and change it to make it do what you wish.
    - The freedom to redistribute copies so you can help your neighbor.
    - The freedom to improve the program, and release your improvements (and modified versions in general) to the public, so that the whole community benefits.
You have the full possibility to install your own version of "Libervia" on your own machine, to verify - and understand - how it works, adapt it to your needs, and share the knowledge with your friends.

- The information regarding the user belong to her, and we will never have the pretention - and indecency ! - to consider the content that she produces or relays via "Libervia" as our property. As well, we commit ourselves to never make profit from selling any of her personal information.

- We greatly encourage a general _decentralisation_. "Libervia" being based on a decentralised protocol (XMPP), it is by nature decentralised. This is essential for a better protection of your information, a better resistance to censorship and hardware or software failures, and to alleviate authoritarian tendencies.

- By fighting against the attempts at private control and commercial abuses of the global network, and trying to remain independent, we are absolutely opposed to any form of advertisement: you will *never* see any advertisement coming from us

- The users Equality is essential for us, we refuse any kind of discrimination, being based on geographical location, population category, or any other ground.

- We will do whatever is possible to fight against any kind of censorship including protecting the speech of victims of harassment, hate speech, threats, humiliation and anything that could lead to self-censorship. The global network must be a means of expression for everyone.

- We refuse the mere idea of an absolute authority regarding the decisions taken for "Libervia" and how it works, and the choice of decentralisation and the use of Libre Software allows to reject all hierarchy.

- The idea of Fraternity is essential. This is why:
    - we will help the users, whatever their computer literacy is, to the extent of what we can
    - we will as well commit ourselves to help the accessibility to "Libervia" for all
    - "Libervia" , XMPP, and the technologies used help facilitate the electronic exchanges, but we strive to focus on real and human exchanges : we will always favor Real on Virtual.

