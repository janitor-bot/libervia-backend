First of all, thank you for helping translating SàT :)

NOTE: *.po files are in i18n directory

To translate a file, you can use a dedicated tool as the excellent gtranslator:

- use the template .pot file (e.g. sat.pot) and name it to your translated language (e.g. fr.po for french); you can preferably generate a new template directly from the source with the following command (eventually adapted, the following command use zsh's globbing syntax), launched from root sat dir after having emptied the build directory:
> xgettext -L python -d sat --keyword=D_ -p i18n  **/*(.py|.tac|primitivus)(.) **/jp(.)

- then to start a new translation, copy i18n/sat.pot to your language file, e.g. cd i18n; cp sat.pot fr.po

- use the choosed tool (a simple text editor can be sufficient) to edit the file: e.g. gtranslator fr.po

- once you translation is finished (or partly finished: the english sentences are used if there is no translation), you can test them by generating a binary and moving it to the right place with the following commands:
> msgfmt -o sat.mo fr.po
> mv sat.mo i18n/fr/LC_MESSAGES/sat.mo

- if you have already a translation, and want to update it (new translations to do, some sentences have changed), you can use the following commands:
> msgmerge fr.po sat.pot > fr2.po
and if everything is allright
> mv fr2.po fr.po

Don't forget that you can (and should !) use the version-control system (mercurial, the "hg" command) to keep history of you translations.

You can check the fr.po file to see how it's done and to know what to put while you set up you translation tool.

Thank you again for you help, don't forget to give me your name and contact email so I can credit you, and don't hesitate to contact me if you need help (goffi@goffi.org, or the sat XMPP room at sat@chat.jabberfr.org).

